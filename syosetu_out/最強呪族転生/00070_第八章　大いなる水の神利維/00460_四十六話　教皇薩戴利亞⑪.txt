佩特羅的話，讓騷動的場面回復寂靜。

我還想佩特羅多少會考慮一下薩戴利亞的提案。然而，聽他的語氣，他一點遲疑都沒有。

薩戴利亞也是，還以為剛才的談話能夠向積極的方向發展，之前一直在高興地笑著的表情一下子灰暗下來。空氣變得很沉重。

「第一，亞貝魯醬有必要守護我這邊的国家呢。被各種滲透攻擊之後，把我們国家的王牌借給利維拉絲国當教皇這種事情，以我的立場是無法允許的」
「⋯⋯確實我国對迪恩那多王国造成了許多麻煩⋯⋯所以才為了解決這個問題，要削弱利維大人的影響力，加強內部的意見統一，我也說過這些。我等如果不是思想鬆散的烏合之眾，而是一個整體的同盟国的話，對於迪恩那多王国來說也絶不是一件壊事。⋯⋯而且，亞貝魯大人要如何選擇，是亞貝魯大人個人的問題，佩特羅大人對此沒有置問的權力，不是嗎？」

薩戴利亞面向我。

「亞貝魯大人會留在利維拉絲国的吧？您剛才是這樣說的吧？您說要幫助我們的国家、民眾⋯⋯」

薩戴利亞纏著我說道。對此，佩特羅作出『什麼都別回答』的眼神示意我。

「你真敢這麼老實地說出來呢。利維拉絲国失去利維，陷入混亂後滅亡，我覺得這樣很好噢。雖然有一部分怨恨迪恩那多王国的人會作出報復，但那是小規模的事情，只要利維騷動能終結，其他的小事無關痛癢呢。這並不是最好的辦法，但也是僅次於最好噢」

⋯⋯佩特羅跟我說的時候，他完全著重於停止來自利維拉絲国的騷擾，但他的真實目的似乎是以己方承受一定傷害為代價的消滅作戰。

「⋯⋯這，難道不是過於執拗的想法嗎？您就是說，即使知道了利維拉絲国的民眾在受苦，自己国內也出現死者，也不願意停手嗎。像我這樣的人，殞命於戰爭之中是當然的報應，但是，利維拉絲国也有日夜盼望著和平、恐懼著內戰的民眾，有好幾十萬人。您真的要做出，這樣像是踐踏螻蟻一樣的事情嗎⋯⋯」
「因為，已經沒有其他選擇了。這個国家已經完蛋了噢。對它作出終結的人就是你喲，薩戴利亞」
「這、這種事情，怎麼可能！我、正是為了避免這個才⋯⋯！」
「你被徒具外表的利維欺騙去立派，並以此幾乎完全統一了各個宗派。確實那個利維是真貨，但即使如此，你的国家所真正信仰的是，在漫長的歷史中積累而成的理想的利維形象，那才是真正的利維教噢」

對於佩特羅的話，薩戴利亞面無表情，站在那裡一動不動，像是凍僵了一般。

「而你以新宗派為主體，以利維拉絲国的大量流血為代價，強行完成統一，將国家信仰的理想形象的聖典否定了。然後你們把替代品強加到聖典並且推行，而那個替代品正是頭腦空洞、早已失去全盛期力量的笨蛋神，真是令人感慨」

薩戴利亞的臉失去血色，變得蒼白。她呼吸粗重，像是馬上要倒下的樣子。

「但你們依舊沒有醒悟，這個笨蛋神背離政治、背離民心，打算從外部引入暴力來解決問題。無論你怎樣努力，內戰永遠都不會休止呢。從根本上就扭曲了，而且還短視，企圖只用蠻力來解決。無需贅言，你們真是做了很不得了的事情呢，薩戴利亞」

薩戴利亞的嘴角微弱地動了動，但是，她什麼話都說不出來。

「我很同情呢。過去的你肯定是將利維視作這個国家變得和平的希望吧。而你今後不再對利維放任不管，將它僅僅作為象徵，其實是很了不起的決定噢。我若是在你的立場的話，肯定是什麼都不說，緊緊跟隨利維的話語吧。這不是謊言，是我的真心話。但是，無論你怎麼掙扎，你為了統一而造成的流血犧牲全都注定是徒勞」

薩戴利亞低下頭，沉默了。對於佩特羅的指謫，她無言以對吧。

說實話，我說不定也小看了佩特羅。我以為他只是個性格惡劣的娘娘腔狂信者而已，沒想到對於那個一直述說著利維拉絲国未來的薩戴利亞，他居然只用語言就把對方完全折服。

『是叫佩特羅嗎？區區人類，竟敢閑話余！余為水之神，是萬古至今的存在！僅僅是活了兩百年的傢伙也敢輕視余，如此愚蠢！簡直是裝腔作勢的青蛙！薩戴利亞喲，這傢伙想要煽動你的不信任，讓你背離我，從而讓這個国家崩壊！不要被這種卑劣的伎倆欺騙噢！』

利維緊逼著薩戴利亞說道。這番話顯然並不是想讓薩戴利亞不被佩特羅牽引，而是要讓她恐懼。

薩戴利亞無助地四周張望，目光停留在我身上。

「亞、亞貝魯大人，成、成為我国的教皇吧⋯⋯我，我以性命請求您⋯⋯」
「不、不是，那個，我⋯⋯」

佩特羅走到我與薩戴利亞之間。

「請不要在繼續聽那邊的笨蛋神的話了。把龍脈停下來吧」

薩戴利亞看到佩特羅後，慢慢地平靜氣息，然後自然地舉起大杖。
不行了，薩戴利亞想要動手。我也把腰上插著的魔杖拔起來。

「在旁邊胡說八道的，不要阻礙我！」

薩戴利亞揮舞大杖。

「বয風喲বহন搬運！」

我把魔杖指向佩特羅。一陣風包裹住佩特羅，把他拉到我的旁邊。，

他剛才站立的地板被空中出現的水塊貫穿。與薩戴利亞的防御結界使用的水同樣，水塊的表面有持續擴散的波紋，浮現著大量的術式。

「⋯⋯果然不行呢。所以我才說別廢話，盡快打倒最好，想起來真是討厭」

佩特羅一邊站起來，一邊小聲嘟囔。

「我承認自己誤入了歧途！但是，若是這樣，只要有亞貝魯大人的魔術的話，不管怎樣都能引導這個国家！這是不會有錯的！亞貝魯大人不願意協助的話，那也沒辦法。把手腳折斷，在您同意之前請一直留在這裡吧！我等的教團裡，儘管我所追求的人才很少，但擅長拷問的人卻很多呢！」

薩戴利亞再次揮動大杖。跟剛才一樣，她要以龍脈的力量使出水之攻擊。

『呼呼呼呼⋯⋯呼哈哈哈哈哈！折斷亞貝魯的手腳麼⋯⋯這真是傑作！很好的判斷，薩戴利亞！果然，必須得要這樣不可！死心吧亞貝魯，余與你這傢伙的恩怨，就在這裡清算！你就小心地對付我的教團裡的危險咒物吧！』

牆壁裂縫裡的利維的身影消失了。下一瞬間，天花板被掀起，利維的巨大手臂揮了下來。