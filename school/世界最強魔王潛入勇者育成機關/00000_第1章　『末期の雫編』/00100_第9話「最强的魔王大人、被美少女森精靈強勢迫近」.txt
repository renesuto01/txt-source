將清澈的空氣滿滿吸入胸中後總感覺掃除了煩惱。

昨天考試後，我粗略地過目了張掛在學園的入學簡章。
『諸位在那個分野顯示最大限度的實力』這好像就是這個學園的理念。正是實力主義其本身。有趣。縱然明擺著我會在全部科目取得第1位，從這個身體中迸發的鬥爭本能還是刺激了我。

儘管現在的我姿態是人類，但裡面是不折不扣的魔神。即使抑制了力量的大部分，魔神的本性也是不會變的。
時間感覺的稀薄和各種各樣的慾望強烈就表現了那個。明明蕾娜是說為我平息，可感覺反倒興奮了。
魔神相愛的話會很長久。正因為生於無限的刻度，所以時間的感覺非常曖昧，一旦埋頭很快就幾天過去了。長的話會花數年。至今為止最忘我地度過的時間是注意到時已經經過了10年。

不行。首先這個『注意到時』必須想個辦法。既然借人的姿態作為人生活，這個就必須想辦法。
……考慮這種事就很累。或許也和最近破壞衝動的高昂有關係。因為平常的我會再稍微冷靜點。

那個時候，到一時興起去看勇者的培育機關為止是一直和露米艾爾度過的。那是也許幾天或者幾年的曖昧。雖然唯獨那是和昨晚與蕾娜度過的時間同等或在其之上的濃密我是記得的。不好打發空閑還興奮著的魔神就是這樣才叫人為難。

「吶，你，你」

說起來，露米艾爾在做什麼呢。反正平常很少有拜訪的人所以隨便地讓她坐在玉座上了，但她有非常沒常性的一面。沒讓不滿爆發就好了……。

「那裡的你」

一旦讓露米艾爾生氣的話就無從下手了。雖然能做到用力量控制住，但做那種事的話她會哭的。雖然狂妄的孩子哭的樣子能勾引人，但我不想看到總是天真開朗的露米艾爾的哭臉。
為了哄她買點什麼土特產回去吧？ 食物或者珠寶飾品嗎。傷腦筋吶。

「喂·喂！！ 聽得見嗎！？」
「嗚哇！？」

我發出了作為魔神的人絕不能發出的悲鳴後跳了起來。心臟快要從嘴裡出來了說的就是這樣嗎！？

「哎，哎呀，你那麼嚇一跳這邊也會為難的……」

向我搭話的少女是森精靈。
深綠色的頭髮放長著，耳朵是像把森精靈的理想就那樣成形了一樣美麗的形狀。
瞳孔是淡綠色。因為現在的我眼睛是像翠玉一樣，所以或許稍微有點像。

外表年齡也就16、7歲吧。身高和我幾乎相同。也許是因為穿著長筒靴。
然後眼睛自然地朝大膽地露出雙肩的上衣，長度非常短的特徵是褶子的深藏青色裙子，和從那裡伸長的像陶瓷器一樣光滑而有彈性的大腿看去了。

啊啊，我已經承認啦。如果沒有行人來往的話在這個時間點我就襲擊了。反正我就是慾望暴露無遺的變態魔神……。
半自暴自棄地一問「什麼？」，森精靈的少女就露出了如花的笑容。

「哎呀～。因為這樣一大清早的，在破舊的旅店前有個一臉無精打采表情的美少年啦～。該說不禁就搭話了嗎」
「哈啊」

就森精靈來說給人相當沒架子的印象。要說我對森精靈的印象的話，就是像昨天的接待小姐一樣總感覺很傲慢看不起人並且散發著不讓其他種族接近的氣氛的感覺。
從這個少女身上感覺不到那個。

「吶吶，我說你。從哪裡來的呢？ 不是這一帶的出身吧？」

森精靈的少女仿彿興致勃勃似的將臉靠過來。不，太近了太近了。我一邊做著像輕輕把她推回去一樣的動作一邊回答。

「嗯嘛，從西方來的吧」

我沒有說謊。真的是從西方來的。因為是從在帝國西方的戴涅布萊魔族國來的呢，嗯。

「西？ 嗯～，那麼是城砦都市格蘭登之類的？」
「啊啊，嘛就是那種感覺吧」

格蘭登是艾伯利亞帝國西面的要衝。很久以前魔族侵攻帝國的時候讓裡面的人類全滅之後作為魔族的據點使用過。
雖然這麼說，但當時的我只是比起帝國倒不如說也有威懾在戴涅布萊正上方的小國雷斯塔弗羅拉聖王國的意義而在玉座上向後靠著。

「呋姆呋姆？ 格蘭登出身？ 那裡大多是金髮的人呢。我覺得像你一樣青髮的男孩子相當罕見吶」

微妙地觸及了吶。被太深入的話就不妙了吧。又不知道現在的格蘭登是怎麼樣。我最後往來那個都市都已經久得想不起來是幾百年前了。

「名字呢？ 你叫什麼？ 告訴我告訴我！」
「……提奧道爾」
「提奧君嗎。吼吼。雖然是美少年但名字感覺很常見呢。啊，不是挖苦你哦～？」
「比起我的事啊」

我把森精靈的少女咚的按在建築物的牆上，一邊握著她的一隻手腕一邊輕輕抬起了她纖細的下巴。

「我各方面比較想知道你的事吶。名字是？」

自尊心很高的森精靈被做這種事的話很可能會激昂或者不快，之前蕾娜這樣告訴過我。為了盡可能快讓她失去對我的興趣這應該是有效果的手法。

「莉茲。可以不加敬稱哦」

莉茲反而頗有意思似的盯著我說道。真是頑強。

「那麼，莉茲。接下來不和我一起去做舒服的事嗎？ 正好這裡是旅店。也有很會響的床最好了喲」

連我自己都覺得下流的邀請方式。有點陷入自我厭惡了。

「舒服的事嗎。可以喲？ 那麼，提奧君要對我做什麼呢？嗯？ 舒服的事，是什麼呢？」

……什麼情況這個森精靈？
我稍微有點焦躁起來了。被說了這樣慾望暴露無遺的話，為什麼不但沒有害怕還強勢地迫近了？

「希望你告訴我吶。舒服的事。還是說」

莉茲身體離開牆上之後反把我按在牆上，用雙手咚的撐著牆。
她可愛的臉就在眼前。

「是不能說出口的事，嗎～？」

冷汗冒出來了。什麼情況，為什麼變成這樣了？ 為什麼立場反轉了？

「好嘛～，說說看啦？ 吶？ 姐姐不會生氣的啦」
「不，不是，那個……」
「那個，什麼？ 嗯～？」

被強勢地靠近了臉，心臟怦怦直跳。怎麼辦？ 這種時候，人類的男人會怎麼辦？

「提奧君，是催逼的時候很強勢但被迫近就會膽怯的類型？ 好可愛！」

莉茲仿彿報復剛才似的用手輕輕抬起了我的下巴。
那淡綠色的瞳孔閃閃發光。

(插圖001)

「吶，最近的森精靈是肉食系的喲？ 被像你一樣的孩子邀請的話會忍不住的。喏喏，趕快進旅店吧？需要的話我來陪你。呋呋呋，讓床嘎吱嘎吱響吧？」

思考停止並凝固了。
被本人這樣說了所以可以吧的思想所驅使。啊啊，如果是平常的自己的話就聽從勸誘了吧，可是現在不行……可惡。

「玩，玩笑開過頭了呢，莉茲」
「嗯？ 我是認真的？」
「不開玩笑我會襲擊的哦？」
「請？ 來吧，來。我是在外面也歡迎的哦？喏喏～」

一動不動地凝視我的眼睛感覺不出是認真的還是開玩笑。怎麼做才是正解的思想在頭腦中縈繞時。
突然旅店的門嘭的開了。

身穿女僕服的銀髮美少女向我們投來了視線。
是蕾娜。而且沒有消去身姿。長相漂亮得和寒磣的旅店過於不相配的女僕少女無言地盯著我們。一想到我被森精靈逼到了牆邊丟人的感情就往上涌了。

「哎呀～，被看見了」

不知是想了什麼，莉茲一下手離開了。我逃似的和她保持了距離。

「明明不用這樣逃的～。稍微認真迫近的話馬上就逃了嗎？ 這種地方也好可愛我很喜歡吶，嗯嗯」
「莉，莉茲……抱歉，我已經差不多得去辦事了」
「我知道啦。學園的入學考試對吧？」

我大吃一驚後看向她。莉茲以不變的笑容說道。

「昨天偶然在學園附近看到了提奧君啦。因為你馬上就進裡面了所以我想是去考試吧。不對嗎？」
「啊，啊～，嗯，對。接下來是去考試哦」
「但是真稀奇呢，3科目都考什麼的。一般1個人就考1科目哦？」

「稍微有點原因」
「呋嗯。嘛，隨便了。其實呢～，我也是今天參加考試」

莉茲撩了下長長的深綠色頭髮後再次看向我。

「那麼我先去等你咯。2個人都努力爭取合格吧？」
「啊，呃，嗯。能那樣就好了呢」

她嫣然一笑後迅速向學園的方向走去了。
我鬆了口氣後回頭看蕾娜。可是蕾娜將食指放在了嘴唇上。別說話，的意思嗎？

『森精靈耳朵很好，會被聽見對話的』

蕾娜像平時一樣在腦內發出響聲。
確實。聽說即便是在各種各樣的種族中森精靈也是特別聽覺出色。雖然這麼說，但因為正好是外出的人數也差不多要變多了的時候所以我覺得沒問題。
蕾娜就那樣一下消去了身影。

『來吧，去學園吧。離考試的時間不太有餘裕了』

是啊。我就那樣前往學園。於是蕾娜對我說道。

『路西法大人不擅長被女性迫近呢。我很清楚』
「唔……」
『那也是因為您那壓倒得有餘的強大的緣故。因為被脆弱的女人迫近的經驗很少。我很欽佩。但是』

蕾娜在這裡斷開後繼續說。

『如果要繼續普通人類的演技的話，對那種女性留神著點比較好。她們在背後會考慮著什麼的。不過，這之後我會好好地守望的所以請放心』

謝謝，蕾娜。已經只有老實地感謝了。
