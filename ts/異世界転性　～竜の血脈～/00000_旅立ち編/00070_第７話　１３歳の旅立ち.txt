木刀和木劍正面相對打著，不斷地較量著。但是雙手拿緊木刀慢慢注入力量。

萊亞斯揮舞起盾牌，就這樣向莉雅這邊跳過來，在那之前，莉雅的木刀從手上脫落。
以為這一意外的動作令萊亞斯的動作一瞬間停下來。那是個致命性的空隙。
我鑽進進他的懷中，捉住他拿著劍的手臂。猛然衝撞到他從肩部到腹部的鐵板的鎧甲上，為了對抗他的力量，我也注進了力氣。

和計算中的一樣！
接下來的瞬間，莉雅的身體以萊亞斯為中心轉了一圈，萊亞斯就這樣背部朝地面倒下去了。

「吥乎」

撿起掉在地面上的木刀，在對方前面打在了他的脖子。沒有錯，莉雅獲勝了咯～

「⋯不⋯難道說是故意把刀掉在哪裡⋯」

萊亞斯稍微過了一會兒就站起來了，臉上浮現出複雜的笑容。
雖然在純粹的劍術上，我還不可能戰勝萊亞斯。但是在對人戰鬥這方面，就是莉雅的勝利。

這個世界有著與魔物的戰鬥和在戰爭中以戰鬥為主的，但是空手格闘術卻沒有發展起來，特別是投技和關節技的發展。
莉雅在前世可是空手道二段，加上柔道二段，還有學習過中國拳法和柔術。作為技能來說，體術等級６。

到現在為止，目標之一也就實現了。

───

莉雅一如既往地從王城溜出來，來到了安卡莎的店。

「我回來了～」

因為現在居住在王城中，所以在進入店時一直總是這樣打招呼。在櫃台的菲，還有坐在櫃台椅子的露露都向這邊轉過臉來。

「歡迎回來」

菲像這樣回答道。而露露露出溫柔的微笑。

「露露，我戰勝萊亞斯囉！」

莉雅自信滿滿地說道，菲則是非常震驚。

「作為王の剣的萊亞斯大人嗎？」
「對啊。嘛，終於可以說是贏了一次」

我坐到了露露的附近。在那裡櫃台的椅子就這樣滿員了。

「媽媽呢？」
「店長的話，去公會發委託」
「比起那個，莉雅，你贏了萊亞斯大人這件事」

露露小心翼翼地詢問道。在這裡的兩人都是，知道莉雅的決心。為了露露而進行的旅行的準備已經好了。

「嗯，準備馬上就好的。我想出去一下」

哈，露露嘆了口氣。

「那麼明天就出發吧」

露露已經從魔法部辭職了。所以暫且在這裡幫助一下阿卡沙的店。
以食物為首的旅行必備的物品是，用莉雅不斷積累的零花錢籌齊的。
然後，她也是想和莉雅一起去旅行。

「我說那個，真的是可以的嗎？這可是與消滅哥布林的情形不同喲？」

因為有過一次得到過幫助。莉雅稍微確認一下她的意思。

「大概是因為父母的血吧。我也想要去出外面旅行」

曾經因為飢餓而差點暴斃街頭的混血精靈，比想像中更加出色啊。
確實作為旅行的同伴，露露是個理想的人才～
因為露露也是接受過盧法斯的魔法教導，所以在國內已經算是相當好的等級了。
比起什麼都是沒有的莉雅，她擁有著，膽小和謹慎等品質。

終於安卡莎回來了，所以四人就這樣圍在餐桌旁。已經結婚有家庭的菲，這一天也是特別在店裡過夜。

───

在半夜莉雅回到了王宮，因為聽到了宮女的抱怨責備，所以就返回到了自己的房間中。

在寢室中打開了步入式衣櫥的附屬門，那裡面被武器裝飾起來。
首先準備魔法袋。想將這個房間的全部東西完全塞滿進去，這是使用時空魔法的貴重品。但是要放進什麼進去果然還是應該嚴格挑選。
禮服之類完全不需要。能夠用來換成金的貴金屬還有盡可能塞滿吧。
王族的紋章是被刻在我所持有的一枚戒指和短刀。
問題是武器啊。
因為在這個世界到現在還是在使用武器，鍛造的技術也是非常高超的。但是大概在江戶時代之後的製造的打擊武器，像是打造洋刀之類是不可能的吧。

在前世有著各種欣賞國寶以及一些重要的文物，關於刀之類的知識可是很了解的。當然的我在實際中確實是使用現代刀來斬殺的。但是古刀等是那麼的美麗，因為在實際中再用來試著斬殺人，聽說過古刀極其鋒利好用。

關於鋼鐵的製法和大刀的製造方法，因為是作為秘傳而不被繼承下來，莉雅對於前世不禁感到很是可惜的說。
鐵刀還是盡可能帶走吧。此外用秘銀做成的刀也是需要的，實際上秘銀刀比起鐵刀在強度和韌性，甚至是鋒利程度等各個方面場合都更好的。而且秘銀是與魔法的親和性高的金屬。
然後只有刀也是不行的。還需要拿走一些劍。並且戰錘，戰斧，長槍，還有小刀等也是盡可能帶走。

莉雅不是有著相當嚴重的收集癖的說，在前世也是只收集一些武器罷了。
畢竟是已經去世的世界所以也是不需要留戀，但是對於那些刀具的收藏到底會變成怎麼樣呢？或許能夠讓渡到知道它們的價值的人手中會比較好的吧。

特別是長曾彌虎徹這把名刀，即使是有多少錢也是買不到的寶器。在入手了那把名刀後的一周時間裡，我一邊笑嘻嘻的傻笑著，一邊更是小心翼翼地欣賞起來。現在回想起來還真是令人毛骨悚然，沒想到日本刀竟然有那樣強大的魔性。

───

第二天黎明時莉雅像以往那樣穿上了出遠門的打扮，帶上了魔法袋。

如同平時那樣，從窗戶出發飛奔進了阿娜亞蘇的街道。在大道路的廣場上，露露正牽著一頭驢站在那裡等著。她的身邊也站著安卡莎。

「媽媽」

我叫道並擁抱了母親。

「大概我說什麼都是沒用的吧，但是可不要亂來哦。也不要給露露添麻煩哦，這是不行的」

是啊，露露到底是擔任著重要的剎車器的角色。不如說那才是她的主要任務。

「我會在兩年之內回來的」
「記得寄信來哦」
「嗯」

互相揮手告別。在黎明時正門也是被打開了，兩人就這樣混入了商人們中走出了阿娜亞蘇。

「那麼，之後該去那裡呢？」
「最初就到迷宮都市去吧」

之前也是那樣說過了。還有旅行的目的等。

隨便說一下，在王宮莉雅的房間裡，我有留下一行紙條。

『想去看看那些比我強大的人』


════════════════════

旅の目的は、バトルだけではないですよ。