祝・コミック一卷發賣＆第６話感想會場
２０１９年 ０３月１６日（土）１４：４３

首先漫畫『黒の魔王』第一卷，正在發售！
單行本收錄了１－５話的內容，在３月２４日之前買的話可以追上連載中的最新話，剛打算開始看的讀者請不要錯過這個機會。
明明２月２２日是發售日，這個活動報告寫得也太遲了，非常抱歉，本來想著６話感想也早點寫出來的⋯⋯儘管如此，總算是寫出來了，請看吧。

關於單行本第一卷的封面。
看到一開始設計的封面就厚顏無恥的要求更換，的場先生真是對不起⋯

因此，這第一卷的封面是經過重新設計之後才完成的。
重設方案有好幾種，單行本的預告圖也是其中的一張。雖然是很棒的一張圖，但是把這個當封面的話一定會讓人以為是和可愛的妖精桑度過安穩的異世界生活之類的故事，完全不能聯繫上黒暗系的標簽。
所以最後決定那張以夕陽為背景的封面是最合適的構圖。平穩生活欺詐的繪圖，我想作為預告圖展示的話也不錯。

關於四格漫畫。
有沒有注意到呢⋯單行本的內封是有附贈的四格漫畫的。這個四格的內容我完全沒有干涉，是的場先生的原創，請一定要好好享受。
說起來，粉絲俱樂部NO１的妖精領袖，是見過莉莉那不祥的誕生場面的，正常來說她更年長。在光之泉裡莉莉算是普通的年輕世代，因此泉裡的平均年齡是⋯⋯妖精和實際年齡無關啦！

第６話感想。
首先，看到標題時我的感想是，

「居然是和戈維納魯的戰鬥⋯⋯」

WEB版非常不受好評＆在書籍版大幅度縮水的第三章，沙利叶ＶＳ戈維納魯。
更不用說在頁碼尺度更加嚴格的漫畫版，戈維納魯戰會砍掉的吧⋯我原本是這麼想的，結果完全是意料之外。
不管怎麼想，和巨大的龍展開華麗的空中戰鬥，作畫上非常麻煩，也很難畫。而且還不是主人公的角色，故事優先度上看來屬於全部砍掉也沒有什麼問題的程度。
即便如此，還是畫了沙利叶ＶＳ戈維納魯，我要表示敬意！
說實話，這個挑戰非常值得感謝，從故事流程的判斷來說，即使砍掉戈維納魯之戰我也不會說「請畫一下」什麼的，還會認為這個選擇更聰明而支持它。
但是，這是提前看到第七使徒沙利叶認真模式的唯一機會，而且葛爾德蘭之戰是非常明確的人間ＶＳ魔族的局面，可以更好的理解世界觀。實際上，看到獨眼巨人什麼的組成軍團戰鬥的場面，會有原來如此，就是這樣的感覺。
無論如何，單純的展現沙利叶這個角色也是最重要的意義。
考慮到她在這個時間點擔當BOSS角色，之後加入女主團，果然還是需要給沙利叶一個表現的機會。雖然想要沙利叶單獨出場機會，但是也不能強行⋯因此，真的在這裡描繪了和戈維納魯的死鬥姿態，我是感激不盡的。

原作讀者大概已經察覺到了，這場戰鬥幾乎是潘多拉大陸的頂上決戰級。
原作最新時間點裡，終於把第七使徒沙利叶在使徒之中的強度序列定了下來，和她能展開互角以上戰鬥的戈維納魯的強度也相對的被定了下來。
沙利叶因為完全沒有使徒固有的特化能力（Exist），被猶達斯評價為「最弱」。這個最弱評價不是總體戰鬥力，而只是從白神獲得加護的強度上的評價。
然而，憑借完美調整之後的人造人的素體，經過殘酷的修行積累充分的實戰經驗，沙利叶的戰鬥能力本身，在使徒之中也是達到了中堅的地位。
這方面，果然米莎和瑪利亞貝爾還太過新人了，戰鬥技術上沙利叶占了壓倒的優勢。不光用槍，還利用作弊的無限魔力經常使用光魔法。
而且，性格非常認真的沙利叶會忠實的執行任務，比起放浪癖的愛，以及其他性格別扭的使徒，在十字教會的評價幾乎是最高的。阿魯斯樞機卿會中意她也是自然的，言聽計從的使徒完全是作弊道具啊。
以上，考慮到諸多因素，第七使徒沙利叶的實力，在全部１２使徒中是第７位，剛好和自己的位階相同。

第一使徒亞當
～～不可超越之壁（神話級）～～
第二使徒亞伯
第三使徒米迦勒・第四使徒約翰奈斯・第五使徒猶大
～～不可超越之壁（傳說級）～～
第六使徒？？？
第七使徒沙利叶←這裡
第八使徒愛・第九使徒？？？・第十使徒？？？
～～不可超越之壁（現役世代）～～
第十一使徒米莎
第十二使徒瑪利亞貝爾
～～最底邊的超新人～～

到現在為止最新話的情報判明的內容，使徒的強度檔次大概就是這個樣子。基本上就和位階的數字一樣。雖然設定上位階不是按強度排的，但是按數字順序來的話更好理解？
並排的是在作品中還沒有體現誰強誰弱的意思。
不用說，？？？是連名字都還沒出現的使徒⋯這種時候？？？一般都是最強的傢伙吧，但是６、９、１０都是相當不上不下位置的傢伙還在保密。
但是，我個人來說，喜歡很早就明確最強的是誰。努力打倒了強敵，卻發現還有更強的存在⋯這個套路雖然方便，續貂的部分很容易戰鬥力過度膨脹。
根據這點，十字教側的第一使徒亞當・第二使徒亞伯就是最強，這樣明示出來的感覺更好。
不管怎麼說，第七使徒沙利叶的實力是第七位，在現役世代的使徒中處於二把手，是有相當的實力了。而能和她對抗的戈維納魯，光是能擋住包括使徒在內的十字軍侵略，可以說是非常強力了。
實際上，現在的元素支配者能贏得了戈維納魯麼？　這樣問的話，大概還不到五五開吧⋯

黒乃打倒第七使徒沙利叶，是因為有陣型『逆十字』削弱使徒加護這種對策，也就是相性上有利。而戈維納魯即使被煉獄困住也不會變弱，黒乃的有利點消失了，所以元素支配者的勝率會下降。
而且，黒乃和莉莉的妖精合體，在煉獄內有壓制沙利叶的實力，是要算上妖精的空中機動能力等等方面，對上戈維納魯也可能比對付沙利叶更靈活的周旋，如果再加上菲奧娜的砲擊支援，火力上也不是無法打倒戈維納魯。不過，妖精合體的時間限制很緊張，戈維納魯如果像某雄火龍一樣逃到時間用盡的話就完了。
所以說，現在的黒乃他們也終於達到和戈維納魯平起平坐的實力了。花了那麼多時間，終於才追上在序盤死掉的角色強度⋯因為戈維納魯太強了。

關於６話的戰鬥場面我完全沒有給出修正的指示。
基本只做了台詞的修正，非要說的話，還有讓『光翼神盾』畫出翅膀一樣的特效，讓戈維納魯的吐息變成黒色這種程度。
不過，考慮台詞，這次也是很辛苦的。
最初的草稿階段，只是普通的「人間風情が」這樣帶侮辱感的台詞，但是戈維納魯這個時候已經輸給了斯巴達王，侮辱人類的台詞就有點奇怪了。
因此，整體上修改成了承認沙利叶是好敵手的台詞。
而且，考慮到戈維納魯的真實身份，怎麼樣都有要加上去的台詞。修正方案裡還配合寫了這樣的說明文字：

※戈維納魯的真實身份，不是純粹的龍而是『？？？』。能說話，能變身成人類（雖然漫畫裡沒有機會）這些能力也是因為他原來是『？？？』的關係。

戈維納魯是知道自己是『？？？』的『？？？』，從沙利叶的名字知道她原來是『？？？』的事，以及古代製作的戰鬥用人造人沙利叶系列（第七使徒沙利叶使用這種人造人的身體製作）的事情，戈維納魯也是知道的。
在漫畫版裡，『來吧，持有天使之名的人類啊！』這個台詞，可能稍微有點唐突的感覺，在原作設定這點上則是無論如何都想讓戈維納魯說出來的。

以上，劇透部分用『？？？』打碼了，這種事也寫了一下。

接下來是關於６話沙利叶的露出。
這不是為了對抗第５話的莉莉⋯實際上一開始沙利叶沒有脫那麼多，也就是說是我讓她脫的。
這是有很深刻的意義的說，最早的草稿裡，沙利叶的露出只有胸口被戈維納魯的爪子劃破那一點點。※胸走光點，特地寫著重要。
之後胸口就一直是破的，本來這套服裝就這麼穿到最後了⋯既然這裡胸走光了，後半段想著多脫一點不是也可以麼？
不對，這不只是單純的殺必死，是更忠實於原作設定的修正方案。
書籍版這裡的戰鬥大幅度縮水沒有什麼特別的描寫，但是原作中沙利叶對戈維納魯是失去了右手右眼的重傷。而且在首都代達羅斯城牆上撞上黒乃，也是因為那天缺損部分再生完了，為了復健出來散步。
由於這種描寫，沙利叶要有重傷的表現才對，因為就是那樣的死鬥，演出表現也是最重要的理由。使徒也要花費全力才能打倒戈維納魯，想要向讀者傳達這種感覺。
因此，這不是為了殺必死而脫，在修正案中加上了手臂和眼睛缺損的描寫要求。
結果，就像大家看到的一樣變成負上名譽的重傷的沙利叶了，手臂從肩膀上都消失了。
在漫畫裡失去的是左手和左眼，和原作相反了，這單純是因為畫格位置時候的姿勢變成了這個樣。
漫畫裡表現了沙利叶正面切開吐息突擊的場面，按原作設定的話這麼幹會被燒成炭的。但是漫畫裡重視視覺效果，吐息突擊也ＯＫ啦。
所以變成為了突破吐息犧牲掉了左半身的畫面。右手持槍，以左半身在前靠左手防御，這樣的感覺。捨身戰法也很有沙利叶的感覺。
不管怎麼說，在書籍版省略掉的重傷沙利叶在漫畫裡畫了出來，個人很滿足。她在被打倒的戈維納魯頭上舉起槍的勝利宣言，我也覺得現在這種破破爛爛重傷半裸的姿態更加神聖。

那麼，這次？？？感覺好像多了點，就到這裡吧。
下次７話菲奧娜終於要登場了，敬請期待！