伊莉斯等人接受委託的養雞農家，位於從桑特魯城出發後步行不到一小時的農村中。委托人原本似乎是高階傭兵，以父親病逝為契機而從傭兵生活中金盆洗手，為了扶養生病的母親而繼承了家裡的農業。
雖然作為傭兵相當有實力的樣子，但光是要賺到足以扶養自己家人的收入，靠當傭兵是很困難的。更遑論還要支付生病的母親的醫療費用，一般人根本無法負擔。
人民為了拯救生病的家人，只能採取抱著自我犧牲的精神被當作奴隷給賣掉，再以這筆錢來補貼醫療費用的作法。這種社會情勢，也可稱得上是某種行政缺失吧。
而他此時想到的，就是販賣狂野咕咕的蛋。
蛋的營養價值高，又被視為高級食材，所以市場需求和售價也很高。想要一舉致富的話，沒有比這更適合的東西了，不過問題就出在這是魔物的蛋上面吧。由於要採收蛋就會被襲擊，成了一場傷痕累累的戰鬥。
然後本次問題的主角狂野咕咕終於變得比飼主還強了。到了這個時候，雖然飼主已經不用再賺取醫療費用了，可是由於牠們化為凶暴的猛獸，讓飼主想過普通的生活都不行。
結果飼主雖然發出了討伐委託，但是狂野咕咕變得太強了。牠們不斷擊退前來挑戰的傭兵，又變成了更強的凶惡雞群。
經過鍛鍊的雞群，成長為連傭兵工會都覺得十分棘手的存在。
「所以妳們三個去挑戰然後被打回來了啊……這還真令人好奇牠們是怎樣的雞呢～♪」
一邊說著農家的狀況，杰羅斯、伊莉斯、嘉內、小楓一行人一邊沿著道路前進。順帶一提，雷娜追著在途中擦身而過的少年傭兵們跑了，下落不明。
大叔非常悠哉，就算一邊抽著菸，腳步仍有些輕快。想必是因為可以從小楓那裡得到醬油吧。
「在下熱血沸騰。真想趕快與之一戰。」
「小楓真的很血氣方剛呢……完全不像是精靈。」
另一方面，小楓正無法抑制自己那熱血的靈魂。
「就快到了，看到那個橘色的屋頂了吧？凶暴的雞就在那裡。」
「在那裡啊……能夠讓在下滿足的勇士就在那裡，真是令人十分期待。」
「小楓妳真的是精靈嗎？精靈前面沒有加上『黑暗』兩字嗎？」
對大叔來說，已經分不清她到底是哪種精靈了。
屬於最上等種族的高階精靈是嗜血至此的野獸。大叔已經有種這世上什麼事情都有可能發生，想要放棄思考的感覺了。大叔體悟到，無論是什麼高階種族，說穿了也只是擁有自我與意識的野獸罷了。
逐漸接近目的地的養雞農家後，他們看到了那個。
一個像是傭兵的男人非常突然地從庭院附近被打飛到天空中。發現到那男人一邊如鑽頭般旋轉著一邊往這邊掉下來時，大叔等人立刻離開了現場。
「嗚啊啊啊啊啊啊啊啊啊啊啊啊啊啊，嗚咕哇哈！」
傭兵頭部朝下的穿破了地面，一邊旋轉一邊挖掘著大地前進。最後有如在某個村子裡發生的慘劇般，幾乎整個人埋沒在地底下，只有腳露了出來。
「這、這招該不會是旋○回旋殺？那不是雞嗎！」
「呵呵呵……這裡有強者的氣息。在下就是為了和比自己更強的對手相會而來！」
「是哪來的格闘家啊！小楓，不可以衝動行事啊！」
四個人無視埋在地底的男人，渾身顫抖。先不提大叔，伊莉絲和嘉內曾一度敗在牠們的手下。要是牠們已經變得比之前更強了，那就代表牠們的成長速度非常快。
狂野咕咕以別種意義上來說的確是魔物。
「這……皮不綳緊一點可不行呢。到底是多強的魔物呢……」
「好想砍，好想揮刀啊……讓在下的太刀嘗嘗鮮血的滋味吧……」
「小楓妳好可怕喔～」
「妳真的是精靈嗎？我已經開始覺得妳是其他種族的了……」
帶著以別種意義上來說渴望著鮮血的野獸，大叔等人踏入了養雞農家。
然而在那裡的，是被破壞得極為悽慘、有如廢墟一般的房子。
庭院裡，被打倒的傭兵們堆成了小山，而在傭兵們上方，無數的雞正以銳利的眼神看著這裡，魄力十足。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【格闘家咕咕】【斬擊咕咕】【狙擊手咕咕】
【白帶咕咕】【弓手咕咕】【劍道咕咕】
狂野咕咕的突然變異進化種。
擁有凌駕於最終進化型態雞蛇的力量，非常好戰的雞。
是特化為狙擊、打擊、斬擊，令人感到驚奇的雞。
包含白帶在內的三種雞順從強者，就像是排名在上的三只雞的弟子。
擁有很高的智能，可以在一定程度上理解人類的話語。肉雖不好吃，蛋卻很美味。
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
「……這不是狂野咕咕耶？該怎麼說，牠們似乎是已經進化過的個體。」
「「啥～？」」
雞群站在被打倒在地的傭兵堆成的小山上瞪著這邊。
就像是不良少年常露出的挑釁眼神，怎麼看牠們都給人一種品行不佳的不良集團的印象。
「唔嗯，在下想與那雙翼有如鋼鐵般熠熠生輝的雞一戰，行麼？」
「不，要對付進化後的個體負擔可是很重的喔？」
「比起那個，那個提出委託的叔叔在哪裡啊？」
大叔先不論，要是委托人不在，伊莉絲等人是無法接下這個工作的。
然而無論在哪裡都找不到重要的委托人。
「沒辦法。既然牠們好像多少聽得懂人話的樣子，就直接問這些雞吧。」
「你認真的嗎……？」
「叔叔，不管怎麼樣事情都不可能會進展得這麼順利的喔？」
「怎樣都無所謂。在下只希望能夠盡快一戰……血……好想見到血啊……」
雖然有個抱持著危險思考的女孩在，但大叔刻意忽視她，站到狂野咕咕的進化種，格闘家咕咕面前。
「你們的飼主在哪裡？不對，應該說是前飼主吧？」
「咕咕……」
格闘家咕咕以翅膀尖端指向被打倒的傭兵。那裡倒著一個有如中元節禮盒裡頭會出現的金華火腿般，整個人腫到不行、滿身是血的大叔。
仔細一看，他似乎是全身都持續遭到強烈的毆打，受到了足以讓全身腫脹的損傷。光是他還活著這點就很令人訝異了。
不知道該說他是運氣好還是不好，依據場合判斷，說不定給他一個痛快對他而言才是救贖。
而且不知為何他頭上的愛心圖樣刺青令人留下了強烈的印象。
「這個人好像是委托人喔？還真虧他沒有爆炸呢……」
「你騙人的吧，之前我們見面的時候他是個肌肉健壯的大叔喔？」
「不可能會腫成這個樣子吧？」
格闘家咕咕從愣住的一行人旁邊走過，以強烈的一腳踢飛了倒在地上的大叔頭部。
「唔咕！努……努們做些……呃模……（你們這些……惡魔……）」
「完全聽不懂他在說什麼呢……真沒辦法，『高級治癒術』。」
「大叔你連回復魔法都會用喔？」
「唉，畢竟是叔叔嘛……會用也是當然的吧～我是沒辦法啦……根本沒學過。是不是該去買卷軸呢？」
受到杰羅斯使用的回復魔法治療，身為委托人的大叔全身上下的腫傷漸漸地復原。
恢復後的樣貌是個肌肉隆隆的光頭大叔。
「波漢先生，你的頭髮怎麼了？之前還很茂密吧？」
「被那些傢伙給拔光了……快點、快點打倒那些傢伙──────！」
「以我個人的立場而言，是希望能夠接收那些雞啦。感覺外出時牠們也能幫忙看家防盜。不過啊……你是真的要哭了啊？牠們有這麼難搞嗎？」
「我的雞……你想要就給你吧。打倒牠們，在這裡將在場的所有雞全都打倒，讓牠們看看你有多強！」
「你看到波漢大叔還有眼前這些堆成小山的傭兵們，還覺得牠們不難搞嗎？」
波漢用像是不知道來自哪裡的海賊王臨死前留下的吶喊般的語氣叫他們處理這些雞。
不過杰羅斯根本不想打倒這些雞。他只是想要會產下美味雞蛋的雞而已。從一開始就完全沒有打算要殺掉牠們，不如說因為在原本的世界裡就有養雞，所以他反而想保護牠們。
不過血氣方剛並在修羅之路上直直前進的高階精靈完全不是這樣想的。
「了解！」
由於原本就想與之一戰，她毫無顧慮的將手伸向背後的太刀，以全速向前奔馳。
對手是斬擊咕咕。牠是雙翼擁有白銀光輝，擅長斬擊的雞。
嘴上叼著牙簽的樣子讓牠意外的頗具威嚴。此外，牠也擁有只要將魔力注入長在雙翼的羽毛上，就能使羽毛化為強韌刀刃的能力。
小楓拔出背上的太刀，迅速地以一招斜砍代替打招呼攻了過去，但是隨著一聲尖銳的「鏘！」，太刀被彈開，小楓的姿勢瞬間變得毫無防備。
斬擊咕咕沒有放過這個機會。牠飛到小楓的身側，有如彎了一個直角似地，以高速拉近彼此間的距離，並用雙翼使出斬擊襲向小楓。
「嘖！」
小楓立刻揮動太刀，配合對方斬擊的軌道，以雙刃微微相接的形式擋開了攻擊，往後一跳，拉開距離。接著她又再度拉近和斬擊咕咕間的距離，同時用太刀施展連續攻擊。



────鏘鏘鏘鏘！鏘嗡嗡嗡！
響起了好幾次的金屬撞擊聲。
一人與一雞的斬擊以令人眼花撩亂的速度碰撞著，銀色的軌跡與火花交互飛舞。
雙方激烈的互相交手。
「雖然小楓也很厲害……但那只雞是怎樣啊～？」
「不管怎麼想那都不是一隻雞該有的強度。那毋庸置疑的是個劍士……」
「突然變異種VS隔代遺傳……還真是場值得一見的勝負啊。」
眼前一進一退的攻防戰仍持續著。
可是大叔此時卻感覺到了些許的氣息，將手伸到了嘉內的頭附近。
「什麼？」
嘉內一瞬間還不知道發生了什麼事，了解狀況的時候臉都白了。
大叔的手上握著一支箭矢。
「……這是狙擊手咕咕幹的好事吧。是從哪裡狙擊的呢……」
「牠不是應該會在箭飛過來的方向嗎？」
「從同一個地點狙擊是三流的狙擊手才會做的事喔。牠應該已經移動到下一個狙擊點去了。」
從狙擊時只感覺到些許的氣息這點來看，這雞恐怕極為擅於隱匿自己的行踪。
既然可以使用弓，那翅膀的骨骼可動範圍應該很廣吧。不如說從骨頭開始就已經不像鳥類，而比較接近人類也說不定。大叔撿起了兩個掉在地上的小石頭，等待著下一次的狙擊。
「嘉內小姐，有辦法復仇嗎？牠們看起來好像不是普通的強耶。」
「沒辦法，我會反過來被牠們打倒吧。完全不覺得有辦法取勝。而且牠們變得比之前更強了……」
「之前是牠們手下留情了？如果是這樣的話我會很失落的～」
小楓雖然不斷反覆地與對方兵刃相向，卻一直無法給予對方關鍵的一擊。同樣的，斬擊咕咕也因為體型小，無法拉近彼此間的差距。
因為體型小，所以動作也快的斬擊咕咕看似占了上風，但小楓也再度以些微的動作化解了斬擊，參雜著反擊在內，擋下了對方的攻擊。
一進一退的攻防戰以令旁人的目光無法追上的驚人速度持續著，雙方都以完全想像不到是小孩子和雞的劍術與速度壓制著彼此。然而仍無法給予對方致命一擊。斬擊咕咕往後方一跳，拉開了距離。
「明明是一隻雞卻有這樣老練的實力……若是人，想必是享譽高名的武士吧。真是遺憾……」
「咕咕、咕咕咕咕！（劍術之路與是鳥或是人無關。妳這話是對敝人的侮辱。）」
「唔，這可真是失禮了。您也是十分像樣的武士呢……在下打從心底向您致歉。」
「咕咕咕咕咕咕咕咕咕咕！（若閣下也是武士，口說無憑，以汝之劍來道盡一切吧。這是身為武者的禮儀。）」
不知為何對話成立了。先不管可以聽懂人話的咕咕們，小楓可以聽懂咕咕的話，是因為高階精靈的特性嗎？儘管兩人（？）之間傳出一種緊張的氣氛，但旁觀者們卻是一頭霧水。
小楓將太刀收入鞘中，擺出拔刀術的架式。斬擊咕咕也像是在回應她似地展開雙翼，擺出獨特的架式。
「恐怕會以這一擊分出勝負吧……」
「啊啊……真是可怕的孩子。小小年紀就有這等技術，這樣繼續成長下去的話還得了啊……」
「那個精靈女孩，比到我家來的傭兵們還要強耶……她到底是何方神聖？」
雙方都靜止不動，儘管正在慢慢拉近彼此的距離、處於一觸即發的氣氛下，仍為了使出這一擊而聚精會神。連旁觀者都不禁屏息守護著他們。緊張的氣氛吞噬了這個空間。
一步，又一步。在他們逐漸逼近彼此的同時，周圍的空氣也愈發沉重。對峙的兩人（？）額上流下了汗水，這短短的時間感覺長得有如永遠一般，雙方都將精神集中到了極限狀態。就如同伊莉絲所說的，這一擊將會分出勝負吧。兩人（？）間的緊張程度達到了最高點。
「……無論勝負都毫無遺憾……」
「咕咕、咕咕咕咕咕咕。（明白！那麼就乾脆地……）」
那一刻即將到來。然而在這緊張的狀況下，大叔察覺到了有什麼在暗中行動。
那是從屋頂的另一側，以進化後有如手臂的翅膀搭著弓的狙擊手。狙擊手咕咕正瞄準了這裡。
「一決高下吧！」「咕咕！（一決高下！）」
在兩人（？）行動的同時，狙擊手咕咕也射出了箭。
牠應該是看準了大家正專注於眼前戰鬥的這個空檔吧，但是大叔以指彈迎擊射來的箭矢，更追加了一發，擊向了因狙擊失敗正準備移動，屋頂上那不識趣的雞。
在狙擊手被反狙擊給擊倒，從屋頂墜往地面的同時，兩位劍鬼的太刀（翅膀）正以超高速交錯。
──鏘──────────！
太刀與翅膀交會，明明是使劍的戰鬥，卻產生了衝擊波。正好站在小楓被彈飛出去方向的杰羅斯將她接個正著。
斬擊咕咕也一樣被彈飛，摔進了同伙的雞群中。
「小楓沒事吧？」
「嗯，她沒事……只是衝擊的力道似乎很強，讓她失去了意識呢。」
「那只雞怎麼樣了……幹掉牠了嗎？」
「還活著喔。仔細看看小楓的太刀吧，雖然是名刀，但沒有開鋒。」
斬擊咕咕也暈了過去，不過臉上似乎帶著十分滿足的笑意。
明明只是只雞，卻意外的是個充滿男子氣概的傢伙。這是題外話，不過狙擊手咕咕瞄準的對象是飼主。不知道是狙擊手的習性使然，還是仍抱有想要殺死對方的恨意，箭尖上還特別用心地塗滿了致死性極高的神經性毒藥。
唯一能知道的，就是狙擊手咕咕是真心想致波漢於死地。
「好了，該打倒的還有一隻……是格闘家咕咕嗎。」
「我沒辦法。雖然想要復仇，但那種戰鬥我可做不來喔？」
「我也不行！因為我是魔導士嘛。」
既然小楓暈了過去，當這只雞的對手任務就肯定會落在大叔身上了。
儘管深深地嘆了一口氣，大叔還是與格闘家咕咕相互對峙。
「可以的話我是希望你們能來我家啦……這樣的話不用戰鬥就可以解決了吧。」
「咕咕咕、咕咕咕咕咕咕咕咕！（看到了那樣的戰鬥後，我也不禁熱血沸騰起來了。就請你陪我過兩招吧。）」
「唉～……真沒辦法……咦、咦～？為什麼我會聽得懂你在說什麼，這是怎麼回事？」
不知為何大叔好像也聽的懂雞的話。這瞬間他仿彿窺見了異世界的神祕之處。
無奈的大叔擺出架式。雖然大叔沒什麼幹勁，但眼前的雞卻興致勃勃地想與人一戰。要是此時拒絕的話想必會傷及對方的自尊，所以只能硬著頭皮當牠的對手了。
不過在對峙的瞬間，大叔便實際感受到格闘家咕咕有多麼強悍。
令人想像不到這是一隻雞所散發出來的霸氣。牠顯然是個強者，而且強度更勝於其他兩只。
一瞬間，格闘家咕咕的身影似乎晃動了一下。
「唔！」
杰羅斯的雙臂交叉，接下了想不到是體型這麼瘦小的雞所發出的強力攻擊。
等他注意到時，格闘家咕咕已經用那像是手臂般的翅膀揍了過來。大叔差點就要順勢被揍飛到後方好幾公尺遠的地方，但硬是在腿上施力，撐了下來。
「看起來完全想像不到會是這麼沉重的一擊……這樣我得久違的拿出真本事才行了呢。」
大叔的眼中寄宿著危險的光芒。「那個時候的杰羅斯」又再度降臨了。
調整呼吸，開始讓魔力──不，讓氣在身體中循環，強化身體能力。他發動了「拳神」的職業技能，能力也開始從魔導士轉變為武鬥家。
魔導士會利用體內的魔力與自然界中的魔力來行使魔法，格闘家則會提升體內的氣並使之循環，借此強化身體的戰鬥能力。由於使用職業特有技巧時會變得無法使用魔法，他現在的身體調整成了專為格闘戰而生的狀態。也就是說現在的大叔不擅長遠距離攻擊，不過只要是魔法使系以外的技能都可以同時並用，所以沒什麼太大問題。
「要上嘍……」
他一個箭步拉近了與格闘家咕咕間的距離後，衝勁不減，就這樣踢出一腿。
格闘家咕咕瞬間向上飛起躲開，而此舉也讓牠暴露在大叔使出的連續攻擊之下。
似乎早已明白此事，格闘家咕咕也使出相同的攻擊迎擊。
──碰！咚、咚咚咚咚咚咚咚咚咚咚咚！
幾乎不可能出現在現實生活中的打擊聲響徹周遭。
出拳、防御、躲開、瞄準破綻，有時強硬地進攻，有時又千鈞一髮地躲開對方的攻擊。
「咕咕！咕咕咕咕咕咕！（好、好強。居然能夠遇到這樣的對手……這是多麼幸運的事啊。）」
「看來你很開心的樣子吶！就這麼想與人一戰嗎！」
「咕咕咕、咕咕咕咕咕咕咕咕咕！（挑戰強者、磨練自己。這就是習武者的宿命啊。）」
「看來你已經做好相當的覺悟了呢！那就讓我見識見識你的力量吧！」
「咕咕咕咕咕！（明白！）」
格闘家咕咕暫時從格闘戰中抽身，拉開了距離後，利用自己的速度產生殘影擾亂對手。以高速逼近並使用強烈的踢擊回敬對手。
而大叔順水推舟似地化開了這波攻勢，發現破綻的瞬間便間不容髮地揮出強烈的拳。然而格闘家咕咕像是算準了這一刻，抓住了他的手臂，想順勢將他摔出去。注意到這點的杰羅斯稍微轉動手臂解開束縛，抽出手臂的同時抓住了格闘家咕咕，一口氣將牠摔往地面。不過格闘家咕咕瞬間扭轉身體，重新調整姿勢，就這樣拍動羽翼，脫離至戰鬥範圍外。
「大、大叔他……會不會太強啦？」
「那種程度對他來說輕而易舉吧。畢竟他是『殲滅者』啊。」
「那是什麼危險的稱號啊……那個大叔到底做了些什麼？」
「很多事情……」
「怎樣都好啦，可是小妹妹……那個已經不是魔導士了吧，他到底是何方神聖啊？」
戰況對格闘家咕咕不利。
但是牠的鬥志不減，不如說還提升了。而且看起來非常開心的樣子。
「喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔！」
「咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕咕！」
兩人再度正面交鋒，以激烈的拳回應彼此。
將氣灌注在雙臂（翅膀）上做強化，互相迎擊彼此的拳頭，生動的打擊聲不絕於耳。
宛如「歐啦歐啦！」或是「喝啊噠噠噠！」那樣某處很有名的故事般，雙方互不相讓地使出仿彿會留下殘影的高速攻擊。那極具威力的拳壓交互飛舞著，已經進入了要是被捲入其中，就算只吃上一擊都不能輕易了事的狀態。
為了不引發這種捲入旁人的悲劇，他們的拳壓互相衝擊抵銷，化為圓形的衝擊波一邊擴散一邊延伸到周圍。
有時他們會拉開彼此間的距離，高高的跳到空中交互踢擊，還一邊在空中出拳，你來我往地落到地面上。
「那個……只要吃上一擊就會死吧？是說大叔不是魔導士嗎？」
「根據我聽說的傳聞，他似乎是極為接近暗殺者的全能高手喔？畢竟是擅長在不知不覺間闖入敵陣中央，利用範圍魔法將敵人給一舉殲滅的魔導士。」
「小妹妹……那個絕對不是魔導士吧。怎麼想都是特種部隊的人吧！」
不斷揮出一擊必殺之拳的大叔，還不知道自己的職業已經被人給否定了。
不過他恐怕也深知自己正是如此，而無法否認吧。因為就算再怎麼說是虛擬的網路世界，他也還是做了不少跟現在類似的事情……
作為補充，由於最後他會徹底的殲滅敵人，所以對於PK職的人來說是十分恐怖的存在。
杰羅斯與雞之間壯烈的互毆一直持續到了日落時分。

◇　◇　◇　◇　◇　◇　◇

太陽下山，世界開始被夜幕籠罩之時，格闘家咕咕由於不斷使出全力應戰的緣故用盡了力氣。然而表情卻十分滿足似地，毫無半點陰霾，像是將自己的一切都用上了，露出了打從心底發出的爽朗笑容（？）。
相對的，大叔連一滴汗都沒流下，反而對自己的體力究竟有多麼不合常理這點感到恐懼。儘管如此他也是認真的與格闘家咕咕一戰。
只是到了後半他便注意到自己的不對勁之處，察覺到這樣下去無法分出勝負。
明明他是打算使出全力的，卻不知為何仍保有可以冷靜地觀察並思考狀況的餘裕。
光是去想自己要是真要出手會是什麼情形，他就感到暈眩。
而進化後的雞群五體投地跪拜在這樣的大叔面前，表示服從。
「這……是表示你們願意服從於我嗎？」
「咕咕！（正是！）」
「咕咕咕咕、咕咕！（我們十分感佩於閣下您的強悍。還請您指導我等。）」
「咕咕、咕咕咕咕咕咕咕咕。（我一擊必殺的招式居然被破解了……我的修行還不夠。）」
從狂野咕咕進化而來的雞群擁有尊崇強者的習性。
一旦拜人為師，便會服從師傅到自身已鍛鍊至足以自立的強度後才離巢。
最終會自己創造新的雞群，將習得的技術傳承下去，永無止境地變得更強。
以某方面來說是比雞蛇性格更差勁，最糟的魔物。
「唉，反正可以收下未受精蛋的話我是沒差啦。畢竟我也想要蛋，而且我有鑑定技能，可以分辨受精蛋跟未受精蛋。」
這些雞群之所以叛變的理由就是這個。
原本蛋就是用來傳宗接代的東西，生下來的蛋中有受精蛋與未受精蛋兩種蛋。
如果是未受精蛋的話便無法孵出小雞，被拿去當成食物也無所謂。但是受精蛋是可以孵出小雞的，所以受精蛋被奪走對於雞群來說可是攸關生死的問題。
波漢連這種基礎知識都不知道，毫不在意地收走所有的蛋，所以才會失去在場雞群的信賴，導致最後引發了叛變。真要說起來，這是孩子被奪走的父母的復仇吧。就算這裡是弱肉強食的世界，無論哪種野獸都是很疼愛自己的孩子的。
而且擁有鑑定技能的大叔也不用擔心會被雞群給怨恨。
「波漢先生，飼料要怎麼辦？」
「你如果告訴我你家在哪的話，我可以送過去。這些傢伙不在了以後，我這次要養牛。」
「波漢大叔，你這是話感覺是在找死喔……」
「無論如何，在下的劍術對手都會住在近鄰嗎。值得一會。」
「小楓……妳還想打啊？」
血氣方剛的高階精靈得到了斬擊咕咕這個對手，往修羅之路上更向前邁進了一步。包含大叔在內的三個人只能無奈嘆氣。
這一天，杰羅斯獲得了十三只雞。牠們會提供雞蛋作為代價，接受大叔的指導。同時也獲得了可以幫忙看家的強力警衛。
這些凶惡的雞的目標到底在哪裡，想要發展到什麼地步仍是個謎。
唯一知道的，就是牠們會以成為最強的野獸為目標，不斷鍛鍊自己這件事。

◇　◇　◇　◇　◇　◇　◇

到家時外面已經完全被夜色給籠罩，舊市區周邊一片寂靜。
杰羅斯以魔石燈點亮屋內，坐在隨意擺放的椅子上。
十三只雞住在杰羅斯家的庭院，雖然他們會提供雞蛋，但事到如今杰羅斯反而對於這樣是否真的不會出現被害者一事感到有些不安。
因為牠們擁有可以溝通的智能，所以大叔打算之後再慢慢教他們，總之先來準備吃晚餐。這時他發現長袍上沾有長長的頭髮。
色澤清淺通透的頭髮是在抱住小楓時沾上的吧，然而問題並不在那裡。
「唔嗯，已經有『變魔種』、『高階精靈的頭髮』了。只要湊齊剩下的素材，之後再用我的血施加咒印，就能夠創造出人工生命體了……」
想要製造出人工生命體的話材料還不夠。還需要貴重的素材「精靈結晶」才能製作。
「『邪神魂魄』……要不要對四神做些惡作劇，關鍵就在這上頭了呢。到底該怎麼辦呢～唉，這等拿到『精靈結晶』之後再來煩惱好了……反正還有時間。」
大叔並沒有原諒這個世界的神所作的事情。
雖然轉生到了這個世界，但那充其量也只是對原有世界存在的神的道歉，像是因為被抱怨了所以沒辦法才這樣對應，可以隱約看見四神其實做事相當隨便的一面。
而且雖然讓他們轉生了，那些擔心卻都是假的，杰羅斯被丟在了有凶惡魔物生息的魔之領域。既然要轉生的話，明明讓所有被害者出現在同一個地方就好了，然而這卻給人一種四神恐怕是基於「我們都讓你們作弊了，這樣正好吧」的玩樂心態才做出這種行為的感覺。
當然也有像伊莉絲這樣對這個世界樂在其中的人，所以杰羅斯也無意隨意掀起戰亂。但就算如此，他也不打算就這樣算了。
「還真是令人煩惱啊……呵呵呵。」
杰羅斯露出了前所未有的伶俐笑意。而這笑容上籠罩著平常未顯露出的惡意。
像是呼應他暗藏的惡意一般，地下某個以金屬制成的機械發出了令人不快的響聲。他早已做好準備，只要有素材就能行動。
一切都看狀況將會如何發展。